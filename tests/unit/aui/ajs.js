'use strict';

import createElement from '../../../src/js/aui/create-element';

describe('aui/ajs', function () {
    it('globals', function () {
        expect(AJS).to.be.a('function');
        expect(AJS).to.equal(createElement);
    });
});
