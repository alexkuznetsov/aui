'use strict';

import clone from '../../../src/js/aui/clone';

describe('aui/clone', function () {
    it('globals', function () {
        expect(AJS.clone).to.equal(clone);
    });
});
