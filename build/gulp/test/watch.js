'use strict';

var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var karma = require('../../lib/karma');
var rootPaths = require('../../lib/root-paths');

var taskBuildTest = gat.load('test/build');

module.exports = gulp.series(
    function watch (done) {
        var srcPaths = rootPaths('src/**');
        var testPaths = rootPaths('tests/**');
        gulp.watch(srcPaths.concat(testPaths), taskBuildTest).on('change', galv.cache.expire);
        done();
    },
    function run (done) {
        return taskBuildTest().on('end', function () {
            karma(gat.opts({
                watch: true
            }), done).start();
        });
    }
);
