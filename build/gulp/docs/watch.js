var assign = require('object-assign');
var docsOpts = require('../../lib/docs-opts');
var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');
var path = require('path');

var opts = gat.opts();
var taskDocs = gat.load('docs');

module.exports = gulp.series(
    taskDocs,
    function docsWatch (done) {
        gulp.watch([
            path.join(opts.root, 'docs/**'),
            path.join(opts.root, 'src/**'),
            'docs/**',
            'src/**'
        ], taskDocs).on('change', galv.cache.expire);
        done();
    },
    function docsWatchServe () {
        opts = assign(docsOpts, opts);
        return gulp.src('.tmp')
            .pipe(gulpWebserver({
                host: opts.host,
                livereload: false,
                open: './docs/dist',
                port: opts.port
            }));
    }
);
