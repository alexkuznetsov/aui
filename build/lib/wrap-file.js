var fs = require('fs');
var wrap = require('./wrap');

module.exports = function (before, after) {
    return function (path, dest) {
        var contents = fs.readFileSync(path);
        fs.writeFileSync(dest || path, wrap(before, after)(contents));
    };
};
